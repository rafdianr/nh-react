import React, { useState } from "react";
import "../assets/style/FooterBottom.css";
import FooterTop from "../components/FooterTop";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import {
  faFacebookF,
  faInstagram,
  faLinkedinIn,
  faTwitter,
} from "@fortawesome/free-brands-svg-icons";

const Footer = () => {
  const [list1] = useState([
    {
      title: "Layanan",
      item: [
        "Domain",
        "Shared Hosting",
        "Cloud VPS Hosting",
        "TransferHosting",
        "Web Builder",
        "Keamanan SSL/HTTPS",
        "Jasa Pembuatan Website",
        "Program Afiliasi",
        "Whois",
        "Niagahoster Status",
      ],
    },
  ]);

  const [list2] = useState([
    {
      title: "Service Hosting",
      item: [
        "Hosting Murah",
        "Hosting Indonesia",
        "Hosting Singapore SG",
        "TransferHosting",
        "Hosting Wordpress",
        "Email Hosting",
        "Reseller Hosting",
        "Web Hosting Unlimited",
      ],
    },
  ]);

  const [list3] = useState([
    {
      title: "Kenapa Pilih Niagahoster",
      item: [
        "Hosting Terbaik",
        "Datacenter Hosting Terbaik",
        "Domain Gratis",
        "Transfer Hosting",
        "Bagi-bagi Domain Gratis",
        "Bagi-bagi Hosting Gratis",
        "Review Pelanggan",
      ],
    },
  ]);

  const [list4] = useState([
    {
      title: "Tutorial",
      item: [
        "Ebook Gratis",
        "Knowledgebase",
        "Blog",
        "Cara Pembayaran",
        "Niaga Course",
      ],
    },
  ]);

  const [list5] = useState([
    {
      title: "Tentang Kami",
      item: [
        "Tentang",
        "Penawaran & Promo Spesial",
        "Niaga Poin",
        "Karir",
        "Kontak Kami",
      ],
    },
  ]);

  return (
    <footer id="footer" className="nh-footer">
      <div className="container">
        <div className="footer-wrap">
          <div className="top">
            <h5 className="footer-title">Hubungi Kami</h5>
            <p className="footer-contact">
              Telp: 0274-2885822
              <br />
              WA: 0895422447394
              <br />
              Senin - Minggu
              <br />
              24 Jam Non Stop
            </p>
            <p className="footer-address">
              Jl. Palagan Tentara Pelajar
              <br />
              No 81 Jongkang, Sariharjo,
              <br />
              Ngaglik, Sleman
              <br />
              Daerah Istimewa Yogyakarta
              <br />
              55581
            </p>
          </div>
          <FooterTop list={list1} />
          <FooterTop list={list2} />
          <FooterTop list={list3} />
        </div>
        <div className="footer-wrap center-wrap">
          <FooterTop list={list4} />
          <FooterTop list={list5} />
          <div className="top">
            <h5 className="footer-title">Newsletter</h5>
            <form action="" className="footer-newsletter">
              <input
                className="newsletter-input"
                name="email"
                type="text"
                placeholder="your email@gmail.com"
              />
            </form>
            <div className="newsletter-btn">
              <button className="news-btn">BERLANGGANAN</button>
            </div>
          </div>
          <div class="top">
            <ul class="social-media">
              <li>
                <a
                  class="icon-fb"
                  href="https://www.facebook.com/niagahoster"
                  target="blank"
                >
                  <i class="fab" alt="facebook">
                    <FontAwesomeIcon icon={faFacebookF} />
                  </i>
                </a>
              </li>
              <li>
                <a
                  class="icon-ig"
                  href="https://www.instagram.com/niagahoster.id"
                  target="blank"
                >
                  <i class="fab" alt="instagram">
                    <FontAwesomeIcon icon={faInstagram} />
                  </i>
                </a>
              </li>
              <li>
                <a
                  class="icon-li"
                  href="https://www.linkedin.com/company/niagahoster"
                  target="blank"
                >
                  <i class="fab" alt="linkedin">
                    <FontAwesomeIcon icon={faLinkedinIn} />
                  </i>
                </a>
              </li>
              <li>
                <a
                  class="icon-twitter"
                  href="https://twitter.com/niagahoster"
                  target="blank"
                >
                  <i class="fab" alt="twitter">
                    <FontAwesomeIcon icon={faTwitter} />
                  </i>
                </a>
              </li>
            </ul>
          </div>
        </div>
        <div className="footer-wrap bottom-wrap">
          <div className="bottom-left">
            <p>
              Copyright ©2019 Niagahoster | Hosting powered by PHP7, CloudLinux,
              CloudFlare, BitNinja and DC DCI-Indonesia. Cloud VPS Murah powered
              by Webuzo Softaculous, Intel SSD and cloud computing technology
            </p>
          </div>
          <div className="bottom-right">
            <div className="privacy-policy">
              <a href="#footer">Syarat dan Ketentuan </a>
              <span> | </span>
              <a href="#footer"> Kebijakan Privasi</a>
            </div>
          </div>
        </div>
      </div>
    </footer>
  );
};

export default Footer;
