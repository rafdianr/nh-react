import React from "react";
import { Link } from "react-router-dom";
import "../assets/style/NiagaNavbar.css";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import {
  faPhone,
  faCommentAlt,
  faShoppingCart,
} from "@fortawesome/free-solid-svg-icons";
import NhLogo from "../assets/images/nh-logo.svg";

const NiagaNavbar = () => {
  return (
    <section className="navbar">
      <div className="container">
        <ul className="navbar-top">
          <li className="topnav">
            <span>
              <FontAwesomeIcon icon={faPhone} />
            </span>
            0274-2885822
          </li>
          <li className="topnav">
            <span>
              <FontAwesomeIcon icon={faCommentAlt} />
            </span>
            Live Chat
          </li>
          <li className="cart">
            <span className="fas fa-shopping-cart">
              <FontAwesomeIcon icon={faShoppingCart} />
            </span>
          </li>
        </ul>
        <div className="navbar-bottom">
          <Link className="logo-link" to="/">
            <img className="nh-logo" src={NhLogo} alt="logo" />
          </Link>
          <div className="navbar-bottom-list">
            <ul className="navbar-list">
              <li className="nv-list">
                <Link to="/unlimited" className="nv-list-item">
                  UNLIMITED HOSTING
                </Link>
              </li>
              <li className="nv-list">
                <Link to="/cloud" className="nv-list-item">
                  CLOUD HOSTING
                </Link>
              </li>
              <li className="nv-list">
                <Link to="/vps" className="nv-list-item">
                  CLOUD VPS
                </Link>
              </li>
              <li className="nv-list">
                <Link to="/domain" className="nv-list-item">
                  DOMAIN
                </Link>
              </li>
              <li className="nv-list">
                <Link to="/afiliasi" className="nv-list-item">
                  AFILIASI
                </Link>
              </li>
              <li className="nv-list">
                <Link to="/blog" className="nv-list-item">
                  BLOG
                </Link>
              </li>
              <li className="nv-login">
                <Link to="/login" className="nv-list-login">
                  LOGIN
                </Link>
              </li>
            </ul>
          </div>
        </div>
      </div>
    </section>
  );
};

export default NiagaNavbar;
