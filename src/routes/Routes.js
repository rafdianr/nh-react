import React, { Fragment } from "react";
import { Route } from "react-router-dom";
import NiagaNavbar from "../layout/NiagaNavbar";
import Footer from "../layout/Footer";
import HomePage from "../pages/HomePage";
import UnlimitedPage from "../pages/UnlimitedPage";
import CloudPage from "../pages/CloudPage";
import VpsPage from "../pages/VpsPage";
import DomainPage from "../pages/DomainPage";
import AfiliasiPage from "../pages/AfiliasiPage";
import BlogPage from "../pages/BlogPage";
import LoginPage from "../pages/LoginPage";

const Routes = () => {
  return (
    <Fragment>
      <NiagaNavbar />
      <Route path="/" component={HomePage} exact />
      <Route path="/unlimited" component={UnlimitedPage} exact />
      <Route path="/cloud" component={CloudPage} exact />
      <Route path="/vps" component={VpsPage} exact />
      <Route path="/domain" component={DomainPage} exact />
      <Route path="/afiliasi" component={AfiliasiPage} exact />
      <Route path="/blog" component={BlogPage} exact />
      <Route path="/login" component={LoginPage} exact />
      <Footer />
    </Fragment>
  );
};

export default Routes;
