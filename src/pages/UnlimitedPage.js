import React, { Fragment } from "react";
import "../assets/style/NewPage.css";

const UnlimitedPage = () => {
  return (
    <Fragment>
      <section class="new-page">
        <div class="container-new">
          <h1>UNLIMITED HOSTING PAGE</h1>
        </div>
      </section>
    </Fragment>
  );
};

export default UnlimitedPage;
