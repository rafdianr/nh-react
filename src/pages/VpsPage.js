import React, { Fragment } from "react";
import "../assets/style/NewPage.css";

const VpsPage = () => {
  return (
    <Fragment>
      <section class="new-page">
        <div class="container-new">
          <h1>CLOUD VPS PAGE</h1>
        </div>
      </section>
    </Fragment>
  );
};

export default VpsPage;
