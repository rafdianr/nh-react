import React, { Fragment } from "react";
import "../assets/style/NewPage.css";

const CloudPage = () => {
  return (
    <Fragment>
      <section class="new-page">
        <div class="container-new">
          <h1>CLOUD HOSTING PAGE</h1>
        </div>
      </section>
    </Fragment>
  );
};

export default CloudPage;
