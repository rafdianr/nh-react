import React, { Fragment } from "react";
import "../assets/style/NewPage.css";

const DomainPage = () => {
  return (
    <Fragment>
      <section class="new-page">
        <div class="container-new">
          <h1>DOMAIN HOSTING PAGE</h1>
        </div>
      </section>
    </Fragment>
  );
};

export default DomainPage;
