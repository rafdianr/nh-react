import React, { Fragment } from "react";
import HomeHero from "../components/HomeHero";
import NiagaSukses from "../components/NiagaSukses";
import NiagaGaransi from "../components/NiagaGaransi";
import NiagaPrioritas from "../components/NiagaPrioritas";
import NiagaHemat from "../components/NiagaHemat";
import NiagaServices from "../components/NiagaServices";
import NiagaTestimoni from "../components/NiagaTestimoni";
import NiagaPackage from "../components/NiagaPackage";
import NiagaFaq from "../components/NiagaFaq";

const HomePage = () => {
  return (
    <Fragment>
      <HomeHero />
      <NiagaServices />
      <NiagaPrioritas />
      <NiagaHemat />
      <NiagaTestimoni />
      <NiagaPackage />
      <NiagaFaq />
      <NiagaGaransi />
      <NiagaSukses />
    </Fragment>
  );
};

export default HomePage;
