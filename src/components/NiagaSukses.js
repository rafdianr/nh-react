import React from "react";
import "../assets/style/NiagaSukses.css";

const NiagaSukses = () => {
  return (
    <section className="nh-sukses">
      <div className="container">
        <div className="sukses-contents">
          <div className="sukses-text">
            <h3>Awali kesuksesan online Anda bersama Niagahoster!</h3>
          </div>
          <div className="sukses-btn">
            {/* nanti diedit lagi untuk css btn nya pake css yg sama dengan btn yg lainnya */}
            <button className="hero-btn">MULAI SEKARANG</button>
          </div>
        </div>
      </div>
    </section>
  );
};

export default NiagaSukses;
