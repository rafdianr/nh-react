import React from "react";
import "../assets/style/NiagaGaransi.css";
import LogoGaransi from "../assets/images/icons-guarantee.svg";

const NiagaGaransi = () => {
  return (
    <section className="nh-garansi">
      <div className="container">
        <div className="garansi-contents">
          <div className="garansi-logo">
            <img src={LogoGaransi} alt="" />
          </div>
          <div className="garansi-text">
            <h3 className="garansi-heading">Garansi 30 Hari Uang Kembali</h3>
            <p className="garansi-description">
              Tidak puas dengan layanan hosting Niagahoster? Kami menyediakan
              garansi uang kembali yang berlaku 30 hari sejak tanggal pembelian.
            </p>
            <button className="hero-btn">MULAI SEKARANG</button>
          </div>
        </div>
      </div>
    </section>
  );
};

export default NiagaGaransi;
