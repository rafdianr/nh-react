import React, { useState } from "react";
import "../assets/style/NiagaTestimoni.css";
import TestimoniCard from "./TestimoniCard";

const NiagaTestimoni = () => {
  const [testimoni] = useState([
    {
      id: 1,
      owner: "Didik & Johan",
      img: require("../assets/images/devjavu.webp"),
      description:
        "Website itu sangat penting bagi UMKM sebagai sarana promosi untuk memenangkan persaingan di era digital.",
      place: "Owner Devjavu",
    },
    {
      id: 2,
      owner: "Bob Setyo",
      img: require("../assets/images/optimizer.webp"),
      description:
        "Bagi saya Niagahoster bukan sekadar penyedia hosting, melainkan partner bisnis yang bisa dipercaya.",
      place: "Owner Digital Optimizer Indonesia",
    },
    {
      id: 3,
      owner: "Budi Seputro",
      img: require("../assets/images/sateratu.webp"),
      description:
        "Solusi yang diberikan tim support Niagahoster sangat mudah dimengerti buat saya yang tidak paham teknis.",
      place: "Owner Sate Ratu",
    },
  ]);
  return (
    <section className="nh-testimoni">
      <div className="container">
        <h3 className="testimoni-heading">
          Kata Pelanggan Tentang Niagahoster
        </h3>
        <TestimoniCard card={testimoni} />
      </div>
    </section>
  );
};

export default NiagaTestimoni;
