import React from "react";
import "../assets/style/NiagaFaq.css";

const NiagaFaq = () => {
  return (
    <section className="nh-faq">
      <div className="container">
        <h3 className="faq-heading">Pertanyaan yang Sering Diajukan</h3>
        <div className="faq-question">
          <div className="question-wrapper">
            <h3 className="question">Apa itu web hosting?</h3>
          </div>
          <div className="question-wrapper">
            <h3 className="question">
              Mengapa saya harus menggunakan web hosting Indonesia?
            </h3>
          </div>
          <div className="question-wrapper">
            <h3 className="question">
              Apa yang dimaksud dengan Unlimited Hosting di Niagahoster
            </h3>
          </div>
          <div className="question-wrapper">
            <h3 className="question">
              Paket web hosting mana yang tepat untuk saya?
            </h3>
          </div>
          <div className="question-wrapper">
            <h3 className="question">
              Apakah semua pembelian hosting mendapatkan domain gratis?
            </h3>
          </div>
          <div className="question-wrapper">
            <h3 className="question">
              Jika sudah memiliki website, apakah bisa transfer web hosting ke
              Niagahoster?
            </h3>
          </div>
          <div className="question-wrapper">
            <h3 className="question">
              Apa saja layanan web hosting Niagahoster?
            </h3>
          </div>
        </div>
      </div>
    </section>
  );
};

export default NiagaFaq;
