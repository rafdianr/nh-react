import React, { Fragment } from "react";

const ServicesCard = (props) => {
  const card = props.card.map((item) => (
    <Fragment key={item.id}>
      <li className="services-card">
        <img src={item.img} alt={item.id} />
        <h4>{item.title}</h4>
        <p className="services-description">{item.description}</p>
        <p className="services-start">Mulai dari</p>
        <p className="services-price">{item.price}</p>
      </li>
    </Fragment>
  ));

  return <ul className="services-list">{card}</ul>;
};

export default ServicesCard;
