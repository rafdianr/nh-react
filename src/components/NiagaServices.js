import React, { useState } from "react";
import "../assets/style/NiagaServices.css";
import ServicesIcon5 from "../assets/images/home-pembuatan-website.svg";
import ServicesCard from "./ServicesCard";

const NiagaServices = () => {
  const [services] = useState([
    {
      id: 1,
      title: "Unlimited Hosting",
      img: require("../assets/images/icon-1.svg"),
      description: "Cocok untuk website skala kecil dan menengah",
      price: "Rp 10.000,-",
    },
    {
      id: 2,
      title: "Cloud Hosting",
      img: require("../assets/images/icons-cloud-hosting.svg"),
      description:
        "Kapasitas resource tinggi, fully managed, dan mudah dikelola",
      price: "Rp 150.000,-",
    },
    {
      id: 3,
      title: "Cloud VPS",
      img: require("../assets/images/icons-cloud-vps.svg"),
      description:
        "Dedicated resource dengan akses root dan konfigurasi mandiri",
      price: "Rp 104.000,-",
    },
    {
      id: 4,
      title: "Domain",
      img: require("../assets/images/icons-domain.svg"),
      description: "Temukan nama domain yang Anda inginkan",
      price: "Rp 14.000,-",
    },
  ]);
  return (
    <section className="nh-services">
      <div className="container">
        <h3 className="heading-services">Layanan Niagahoster</h3>
        <ServicesCard card={services} />
        <div className="services-extra">
          <div className="services-extra-list">
            <div className="extra-card">
              <img className="extra-icon" src={ServicesIcon5} alt="" />
              <div className="extra-description">
                <h3>Pembuatan Website</h3>
                <p>
                  500 perusahaan lebih percayakan pembuatan websitenya pada
                  kami.
                </p>
              </div>
            </div>
          </div>
        </div>
      </div>
    </section>
  );
};

export default NiagaServices;
