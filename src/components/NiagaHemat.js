import React from "react";
import "../assets/style/NiagaHemat.css";
import HargaMurah from "../assets/images/icons-domain-harga-murah.svg";
import SelaluOnline from "../assets/images/icons-website-selalu-online.svg";
import SupportAndal from "../assets/images/icons-domain-support-andal.svg";
import HematImg1 from "../assets/images/titis.webp";
import HematImg2 from "../assets/images/man.png";
import HematImg3 from "../assets/images/woman1.webp";
import HematImg4 from "../assets/images/woman2.png";
import HematImg5 from "../assets/images/intercom-logo.svg";
import HematImg6 from "../assets/images/icon-online.svg";

const NiagaHemat = () => {
  return (
    <section className="nh-saving">
      <div className="container">
        <h3 className="saving-heading">Biaya Hemat, Kualitas Hebat</h3>
        <div className="wrap-contents">
          <div className="wrap-images-saving">
            <img className="images-saving-img1" src={HematImg1} alt="" />
            <img className="images-saving-img2" src={HematImg2} alt="" />
            <img className="images-saving-img3" src={HematImg3} alt="" />
            <img className="images-saving-img4" src={HematImg4} alt="" />
            <img className="images-saving-img5" src={HematImg5} alt="" />
            <img className="images-saving-img6" src={HematImg6} alt="" />
          </div>
          <div className="wrap-feature">
            <div className="feature-item">
              <h4>Harga Murah, Fitur Lengkap</h4>
              <img className="feature-item-img" src={HargaMurah} alt="" />
              <p>
                Anda bisa berhemat dan tetap mendapatkan hosting terbaik dengan
                fitur lengkap, dari auto install WordPress, cPanel lengkap,
                hingga SSL gratis
              </p>
            </div>
            <div className="feature-item">
              <h4>Website Selalu Online</h4>
              <img className="feature-item-img" src={SelaluOnline} alt="" />
              <p>
                Jaminan server uptime 99,98% memungkinkan website Anda selalu
                online sehingga Anda tidak perlu khawatir kehilangan trafik dan
                pendapatan.
              </p>
            </div>
            <div className="feature-item">
              <h4>Tim Support Andal dan Cepat Tanggap</h4>
              <img className="feature-item-img" src={SupportAndal} alt="" />
              <p>
                Tidak perlu menunggu lama, selesaikan masalah Anda dengan cepat
                secara real time melalui live chat 24/7
              </p>
            </div>
          </div>
        </div>
      </div>
    </section>
  );
};

export default NiagaHemat;
