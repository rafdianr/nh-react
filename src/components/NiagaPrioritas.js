import React from "react";
import "../assets/style/NiagaPrioritas.css";
import SuperCepat from "../assets/images/hosting-super-cepat.svg";
import DomainKeamanan from "../assets/images/domain-keamanan-ekstra.svg";
import PrioritasImg1 from "../assets/images/server.webp";
import PrioritasImg2 from "../assets/images/graphic.svg";
import PrioritasImg3 from "../assets/images/imunify.svg";
import PrioritasImg4 from "../assets/images/lite-speed.svg";

const NiagaPrioritas = () => {
  return (
    <section className="nh-priority">
      <div className="container">
        <h3 className="priority-heading">Prioritas Kecepatan dan Keamanan</h3>
        <div className="wrap-contents">
          <div className="wrap-feature">
            <div className="feature-item">
              <h4>Hosting Super Cepat</h4>
              <img className="feature-item-img" src={SuperCepat} alt="" />
              <p>
                Pengunjung tidak suka website lambat. Dengan dukungan LiteSpeed
                Web Server, waktu loading website Anda akan meningkat pesat.
              </p>
            </div>
            <div className="feature-item">
              <h4>Keamanan Website Ekstra</h4>
              <img className="feature-item-img" src={DomainKeamanan} alt="" />
              <p>
                Teknologi keamanan Imunify 360 memungkinkan website Anda
                terlindung dari serangan hacker, malware, dan virus berbahaya
                setiap saat.
              </p>
            </div>
            <button className="hero-btn">LIHAT SELENGKAPNYA</button>
          </div>
          <div className="wrap-images">
            <img className="images-img1" src={PrioritasImg1} alt="" />
            <img className="images-img2" src={PrioritasImg2} alt="" />
            <img className="images-img3" src={PrioritasImg3} alt="" />
            <img className="images-img4" src={PrioritasImg4} alt="" />
          </div>
        </div>
      </div>
    </section>
  );
};

export default NiagaPrioritas;
