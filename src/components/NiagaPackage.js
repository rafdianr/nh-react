import React from "react";
import "../assets/style/NiagaPackage.css";
import PackageImg from "../assets/images/icon-package.svg";

const NiagaPackage = () => {
  return (
    <section className="nh-package">
      <div className="container-package">
        <h3 className="package-heading">Pilih Paket Hosting Anda</h3>
        <div className="package-wrap">
          <div className="package-card-wrap">
            <div className="package-card bayi">
              <p className="package-label green-label">Termurah!</p>
              <h5 className="package-title">Bayi</h5>
              <p className="package-price price-hidden">"29.000"</p>
              <p className="package-discount">Rp 10.000</p>
              <div className="package-space"></div>
              <button className="hero-btn">PILIH SEKARANG</button>
              <p className="package-description">
                Sesuai untuk Pemula atau Belajar Website
              </p>
              <ul className="package-feature">
                <li>
                  <div className="package-icon">
                    <img src={PackageImg} alt="" />
                  </div>
                  <p className="package-feature-text">500 MB Disk Space</p>
                </li>
                <li>
                  <div className="package-icon">
                    <img src={PackageImg} alt="" />
                  </div>
                  <p className="package-feature-text">Unlimited Bandwith</p>
                </li>
                <li>
                  <div className="package-icon">
                    <img src={PackageImg} alt="" />
                  </div>
                  <p className="package-feature-text">Unlimited Database</p>
                </li>
                <li>
                  <div className="package-icon">
                    <img src={PackageImg} alt="" />
                  </div>
                  <p className="package-feature-text">1 Domain</p>
                </li>
                <li>
                  <div className="package-icon">
                    <img src={PackageImg} alt="" />
                  </div>
                  <p className="package-feature-text">Instant Backup</p>
                </li>
                <li>
                  <div className="package-icon">
                    <img src={PackageImg} alt="" />
                  </div>
                  <p className="package-feature-text">
                    Unlimited SSL Gratis Selamanya
                  </p>
                </li>
              </ul>
              <a
                className="package-detail"
                href="https://www.niagahoster.co.id/hosting-indonesia#hosting-price"
                target="blank"
              >
                Lihat detail fitur
              </a>
            </div>
          </div>
          <div className="package-card-wrap">
            <div className="package-card bayi">
              <p className="package-label green-label">Diskon up to 34%</p>
              <h5 className="package-title">Pelajar</h5>
              <p className="package-price">Rp 60.800,-</p>
              <p className="package-discount">Rp 40.223</p>
              <div className="package-space"></div>
              <button className="hero-btn">PILIH SEKARANG</button>
              <p className="package-description">
                Sesuai untuk Budget Minimal, Landing Page, Blog Pribadi
              </p>
              <ul className="package-feature">
                <li>
                  <div className="package-icon">
                    <img src={PackageImg} alt="" />
                  </div>
                  <p className="package-feature-text">Unlimited Disk Space</p>
                </li>
                <li>
                  <div className="package-icon">
                    <img src={PackageImg} alt="" />
                  </div>
                  <p className="package-feature-text">Unlimited Bandwidth</p>
                </li>
                <li>
                  <div className="package-icon">
                    <img src={PackageImg} alt="" />
                  </div>
                  <p className="package-feature-text">Unlimited POP3 Email</p>
                </li>
                <li>
                  <div className="package-icon">
                    <img src={PackageImg} alt="" />
                  </div>
                  <p className="package-feature-text">Unlimited Database</p>
                </li>
                <li>
                  <div className="package-icon">
                    <img src={PackageImg} alt="" />
                  </div>
                  <p className="package-feature-text">10 Addon Domain</p>
                </li>
                <li>
                  <div className="package-icon">
                    <img src={PackageImg} alt="" />
                  </div>
                  <p className="package-feature-text">Instant Backup</p>
                </li>
                <li>
                  <div className="package-icon">
                    <img src={PackageImg} alt="" />
                  </div>
                  <p className="package-feature-text">Domain Gratis</p>
                </li>
                <li>
                  <div className="package-icon">
                    <img src={PackageImg} alt="" />
                  </div>
                  <p className="package-feature-text">
                    Unlimited SSL Gratis Selamanya
                  </p>
                </li>
              </ul>
              <a
                className="package-detail"
                href="https://www.niagahoster.co.id/hosting-indonesia#hosting-price"
                target="blank"
              >
                Lihat detail fitur
              </a>
            </div>
          </div>
          <div className="package-card-wrap">
            <div className="package-card bayi">
              <p className="package-label yellow-label">Diskon up to 75%</p>
              <h5 className="package-title">Personal</h5>
              <p className="package-price">Rp 106.250,-</p>
              <p className="package-discount">Rp 26.563</p>
              <div className="package-space"></div>
              <button className="hero-btn">PILIH SEKARANG</button>
              <p className="package-description">
                Sesuai untuk Website Bisnis, UKM, Organisasi, Komunitas, Toko
                Online, dll
              </p>
              <ul className="package-feature">
                <li>
                  <div className="package-icon">
                    <img src={PackageImg} alt="" />
                  </div>
                  <p className="package-feature-text">Unlimited Disk Space</p>
                </li>
                <li>
                  <div className="package-icon">
                    <img src={PackageImg} alt="" />
                  </div>
                  <p className="package-feature-text">Unlimited Bandwidth</p>
                </li>
                <li>
                  <div className="package-icon">
                    <img src={PackageImg} alt="" />
                  </div>
                  <p className="package-feature-text">Unlimited POP3 Email</p>
                </li>
                <li>
                  <div className="package-icon">
                    <img src={PackageImg} alt="" />
                  </div>
                  <p className="package-feature-text">Unlimited Database</p>
                </li>
                <li>
                  <div className="package-icon">
                    <img src={PackageImg} alt="" />
                  </div>
                  <p className="package-feature-text">Unlimited Addon Domain</p>
                </li>
                <li>
                  <div className="package-icon">
                    <img src={PackageImg} alt="" />
                  </div>
                  <p className="package-feature-text">Instant Backup</p>
                </li>
                <li>
                  <div className="package-icon">
                    <img src={PackageImg} alt="" />
                  </div>
                  <p className="package-feature-text">Domain Gratis</p>
                </li>
                <li>
                  <div className="package-icon">
                    <img src={PackageImg} alt="" />
                  </div>
                  <p className="package-feature-text">
                    Unlimited SSL Gratis Selamanya
                  </p>
                </li>
                <li>
                  <div className="package-icon">
                    <img src={PackageImg} alt="" />
                  </div>
                  <p className="package-feature-text">
                    SpamAsassin Mail Protection
                  </p>
                </li>
              </ul>
              <a
                className="package-detail"
                href="https://www.niagahoster.co.id/hosting-indonesia#hosting-price"
                target="blank"
              >
                Lihat detail fitur
              </a>
            </div>
          </div>
          <div className="package-card-wrap">
            <div className="package-card bayi">
              <p className="package-label green-label">Diskon up to 42%</p>
              <h5 className="package-title">Bisnis</h5>
              <p className="package-price">Rp 147.800,-</p>
              <p className="package-discount">Rp 85.724</p>
              <div className="package-space"></div>
              <button className="hero-btn">PILIH SEKARANG</button>
              <p className="package-description">
                Sesuai untuk Website Bisnis, Portal Berita, Toko Online, dll
              </p>
              <ul className="package-feature">
                <li>
                  <div className="package-icon">
                    <img src={PackageImg} alt="" />
                  </div>
                  <p className="package-feature-text">Unlimited Disk Space</p>
                </li>
                <li>
                  <div className="package-icon">
                    <img src={PackageImg} alt="" />
                  </div>
                  <p className="package-feature-text">Unlimited Bandwidth</p>
                </li>
                <li>
                  <div className="package-icon">
                    <img src={PackageImg} alt="" />
                  </div>
                  <p className="package-feature-text">Unlimited POP3 Email</p>
                </li>
                <li>
                  <div className="package-icon">
                    <img src={PackageImg} alt="" />
                  </div>
                  <p className="package-feature-text">Unlimited Database</p>
                </li>
                <li>
                  <div className="package-icon">
                    <img src={PackageImg} alt="" />
                  </div>
                  <p className="package-feature-text">Unlimited Addon Domain</p>
                </li>
                <li>
                  <div className="package-icon">
                    <img src={PackageImg} alt="" />
                  </div>
                  <p className="package-feature-text">
                    Magic Auto Backup & Restore
                  </p>
                </li>
                <li>
                  <div className="package-icon">
                    <img src={PackageImg} alt="" />
                  </div>
                  <p className="package-feature-text">Domain Gratis</p>
                </li>
                <li>
                  <div className="package-icon">
                    <img src={PackageImg} alt="" />
                  </div>
                  <p className="package-feature-text">
                    Unlimited SSL Gratis Selamanya
                  </p>
                </li>
                <li>
                  <div className="package-icon">
                    <img src={PackageImg} alt="" />
                  </div>
                  <p className="package-feature-text">
                    Prioritas Layanan Support
                  </p>
                </li>
                <li>
                  <div className="package-icon">
                    <img src={PackageImg} alt="" />
                  </div>
                  <p className="package-feature-text">
                    SpamAsassin Mail Prrotection
                  </p>
                </li>
              </ul>
              <a
                className="package-detail"
                href="https://www.niagahoster.co.id/hosting-indonesia#hosting-price"
                target="blank"
              >
                Lihat detail fitur
              </a>
            </div>
          </div>
        </div>
      </div>
    </section>
  );
};

export default NiagaPackage;
