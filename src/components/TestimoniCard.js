import React, { Fragment } from "react";

const TestimoniCard = (props) => {
  const card = props.card.map((item) => (
    <Fragment key={item.id}>
      <li>
        <div className="testimoni-image-wrap">
          <img className="testimoni-img" src={item.img} alt={item.owner} />
          <div className="img-overlay">
            <div className="play-btn"></div>
          </div>
        </div>
        <p className="testimoni-description">{item.description}</p>
        <p className="testimoni-name">
          {item.owner} - <span>{item.place}</span>
        </p>
      </li>
    </Fragment>
  ));

  return <ul className="testimoni-list">{card}</ul>;
};

export default TestimoniCard;
