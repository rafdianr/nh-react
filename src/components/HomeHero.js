import React from "react";
import "../assets/style/HomeHero.css";

const HomeHero = () => {
  return (
    <section className="hero">
      <div className="container-hero">
        <div className="hero-left">
          <h1>Unlimited Web Hosting Terbaik di Indonesia</h1>
          <h2>
            Ada banyak peluang bisa Anda raih dari rumah dengan memiliki
            website. Manfaatkan diskon hosting hingga 75% dan tetap produktif di
            bulan Ramadhan bersama Niagahoster.
          </h2>
          <p>Yuk segera order karena diskon dapat berakhir sewaktu-waktu!</p>
          <button className="hero-btn">PILIH SEKARANG</button>
        </div>
        <div className="hero-right">
          <div className="img-rmd"></div>
        </div>
      </div>
    </section>
  );
};

export default HomeHero;
